class BookingMailer < ApplicationMailer
  default from: 'duonghuynhtdt@gmail.com'

  def send_receipt(booking, email)
    @booking = booking
    @email = email
    mail(to: @email, subject: 'Your receipt from KiPi-Hotel')
  end

  def send_destroy(order, email)
    @order = order
    @email = email
    mail(to: @email, subject: 'Your cancel booking from KiPi-Hotel')
  end
end


