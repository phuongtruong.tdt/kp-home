class AddTotalAmountCentsToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :total_amount_cents, :integer, default: 0
  end
end
