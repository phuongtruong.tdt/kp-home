class AddTitleToHotelCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :hotel_categories, :title, :string
  end
end
