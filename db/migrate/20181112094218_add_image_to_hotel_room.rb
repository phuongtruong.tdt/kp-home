class AddImageToHotelRoom < ActiveRecord::Migration[5.0]
  def change
    add_column :hotel_rooms, :image, :string
  end
end
