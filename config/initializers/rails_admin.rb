RailsAdmin.config do |config|

  ### Popular gems integration
   config.main_app_name = ["KiPi", "Hotel"]
  # or something more dynamic
  config.main_app_name = Proc.new { |controller| [ "KiPi", "Hotel - #{controller.params[:action].try(:titleize)}" ] }

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)
  
  config.authenticate_with do
    warden.authenticate! scope: :admin
  end
  config.current_user_method(&:current_admin)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model HotelRoom do
     edit do
      field :name
      field :max_adults
      field :max_children
      field :hotel_category
      field :status
      field :bookings
     end
  end

  config.model HotelCategory do
     edit do
      field :name
      field :image
      field :price
      field :hotel_rooms
      field :title, :ck_editor
     end
  end
end
